DIU Enseigner l'informatique au lycée
=====================================

Univ. Lille

Portail (public) du DIU Enseigner l'informatique au lycée, Université de Lille. Voir aussi http://portail.fil.univ-lille1.fr/fe/diu-eil


Informations pratiques
----------------------

* La formation se déroule à Villeneuve d'Ascq sur le campus cité scientifique de
l'université de Lille. Principalement au bâtiment M5. 
  - voyez [la carte](https://osm.org/go/0B1fzL6bh--?m=) 
  - ou le [plan du campus](https://www.univ-lille.fr/fileadmin/user_upload/autres/Plan-site-Ulille-contact-cite%CC%81-scientifique.pdf)


La formation découpée en cinq blocs se déroule sur deux années :

* 3 blocs sont enseignés cette année 2018/19
* 2 blocs l'année 2019/20

Dates et horaires
-----------------

Année 2018/19 - blocs 1 à 3 

* **réunion de rentrée jeudi 9 mai 2019, 9h**
  * café d'accueil à partir de 8h30 
  * amphi Bacchus, bâtiment M5
* jeudi 9 et vendredi 10 mai 2019
* mercredi 15 mai 2019
* mercredi 5 juin 2019
* du lundi 17 au vendredi 21 juin 2019 
* du lundi 1er au vendredi 5 juillet 2019 
* a priori de 9h à 12h15 et de ~~13h45 à 17h~ 13h15 à 16h30 

Calendrier
----------

Voir la [page dédiée - calendrier](calendrier.md)

* bloc, horaire, salles, intervenants, etc. 

Groupes
-------

Voir la [page dédiée - groupes](groupes/readme.md)

* fichiers émargement par groupe  : [groupe 1](groupes/diu_18-19_groupe1.ods)  / [groupe 2](groupes/diu_18-19_groupe2.ods) / [groupe 3](groupes/diu_18-19_groupe3.ods)

Ressources pédagogiques pour les 5 blocs de la formation
--------------------------------------------------------

* [bloc 1 — Représentation des données et programmation](bloc1/Readme.md)
* [bloc 2 — Algorithmique](bloc2/Readme.md)
* [bloc 3 — Architectures matérielles et robotique, systèmes et réseaux](bloc3/Readme.md)
* [bloc 4 — Programmation avancée et bases de données](bloc4/Readme.md)
* [bloc 5 — Algorithmique avancée](bloc5/Readme.md)

Canaux de discussions Mattermost
--------------------------------

* [mattermost-fil.univ-lille.fr/diu-eil/](https://mattermost-fil.univ-lille.fr/diu-eil/)
  - [canal général](https://mattermost-fil.univ-lille.fr/diu-eil/channels/town-square)
  / [canal bloc 1](https://mattermost-fil.univ-lille.fr/diu-eil/channels/bloc-1) 
  / [canal bloc 2](https://mattermost-fil.univ-lille.fr/diu-eil/channels/bloc-2) 
  / [canal bloc 3](https://mattermost-fil.univ-lille.fr/diu-eil/channels/bloc-3) 

Ressources générales
--------------------

### → réunion de rentrée ###

* Support de présentation, réunion de rentrée mai 2019
  * [version pour projection](doc/2019-06-diueil-slide.pdf) / [version 4 par pages pour impression](doc/2019-06-diueil-4up.pdf) / [(fichier source Markdown)](doc/2019-06-diueil.md)

### → informatique au féminin ###

* Maude Pupin
* informations sur les bourses
  [site université](https://www.univ-lille.fr/vie-des-campus/bourses-et-aides-financieres/bourse-informatique-au-feminin/) - actualisation à venir
* L Codent L Créent - stage juin 2019
	[page d'informations](http://chticode.info/wiki/ecoles/lclc/2019/home)
	/ [flyer](https://wikis.univ-lille1.fr/chticode/_media/wiki/ecoles/lclc/2019/flyer_lclc_stage2019_clair.png)
	/ [formulaire](https://enquetes.univ-lille.fr/index.php/771453)
* [Invitation à l'exposition du 21 juin 16h30](img/2019-06-lclc-expo-invit.png)

### → enjeux environnementaux ###

* _Quelques enjeux écologiques autour du numérique_ - présentation de Peter Sturm du 21 juin
  * [support de présentation](doc/2019-06-ecoinfo.pdf) / [version longue](doc/2019-06-ecoinfo-long.pdf)
* [Tour Inri’à Vélo de Peter Sturm](https://team.inria.fr/steep/tour-inria-velo-de-peter-sturm/)
* Groupe EcoInfo du CNRS [ecoinfo.cnrs.fr/](https://ecoinfo.cnrs.fr/)
* _Introduction aux impacts environnementaux du numérique_,
  Kevin Marquet, Françoise Berthoud, Jacques Combaz,
  * 1024 – bulletin de la Société informatique de France, n°13, avril 2019
  * [sur le site societe-informatique-de-france.fr/](https://www.societe-informatique-de-france.fr/bulletin/1024-numero-13/)
  * [14 pages, PDF, accès direct](https://www.societe-informatique-de-france.fr/wp-content/uploads/2019/04/1024-numero-13_Article19.pdf))

### → qcm classe de 1re ###

* [Formulaire de saisie de questions](https://framaforms.org/qcm-nsi-1re-1561056860)
* voir ci-dessous le programme de classe de 1re, les modalités
  d'épreuves communes de contrôle continu, et le sujet 0

Autres ressources
-----------------

* Programme pédagogique national du DIU EIL
  * [sourcesup.renater.fr/diu-eil/medias/diu-eil-habilit-2-ppn.pdf](https://sourcesup.renater.fr/diu-eil/medias/diu-eil-habilit-2-ppn.pdf) (PDF, 7 pages).
* Programme d'enseignement de spécialité de NSI — numérique et sciences informatiques
  * programme de la classe de première de la voie générale \
	[education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138157](http://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138157) \
    [cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf) (PDF, 9 pages)
  * projet de programme de la classe de terminale de la voie générale \
	[education.gouv.fr/cid131841/elaboration-des-projets-programme-futur-lycee.html](https://www.education.gouv.fr/cid131841/elaboration-des-projets-programme-futur-lycee.html) \
	[cache.media.education.gouv.fr/file/CSP/61/4/Tle_NSI_Specialite_Voie_G_1126614.pdf](https://cache.media.education.gouv.fr/file/CSP/61/4/Tle_NSI_Specialite_Voie_G_1126614.pdf) (PDF, 12 pages)
  * propositions relatives aux épreuves d’examen en numérique et
	sciences informatiques (proposition du groupe d’experts présentée
	aux membres du Conseil supérieur des programmes) \
	[education.gouv.fr/cid131841/elaboration-des-projets-programme-futur-lycee.html](https://www.education.gouv.fr/cid131841/elaboration-des-projets-programme-futur-lycee.html) \
	[cache.media.education.gouv.fr/file/CSP/61/6/Projet_Epreuves_examen_NSI_Tle_Voie_G_1126616.pdf](https://cache.media.education.gouv.fr/file/CSP/61/6/Projet_Epreuves_examen_NSI_Tle_Voie_G_1126616.pdf)
	(PDF, 3 pages)
  * épreuves communes de contrôle continu des enseignements de spécialité suivis uniquement pendant la classe de première de la voie générale - session 2021 \
	[education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=141199](https://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=141199)
  * sujets zéro en classe de première pour le baccalauréat 2021 \
    [eduscol.education.fr/cid141765/sujets-zero-1e-bac-2021.html](https://eduscol.education.fr/cid141765/sujets-zero-1e-bac-2021.html) \
	[cache.media.eduscol.education.fr/file/Annales_zero_BAC_2021_1e/87/9/S0BAC21-1e-SPE-NSI_1133879.pdf](https://cache.media.eduscol.education.fr/file/Annales_zero_BAC_2021_1e/87/9/S0BAC21-1e-SPE-NSI_1133879.pdf)
	(PDF, 11 pages)
* Page vitrine nationale du DIU EIL [sourcesup.renater.fr/diu-eil/](https://sourcesup.renater.fr/diu-eil/)
* _DIU « Enseigner l’informatique au lycée»_,
  Philippe Marquet, Christophe Declercq, pour le comité de pilotage du DIU
  * 1024 – bulletin de la Société informatique de France, n°13, avril 2019
  * [sur le site societe-informatique-de-france.fr/](https://www.societe-informatique-de-france.fr/bulletin/1024-numero-13/)
  * [10 pages, PDF, accès direct](https://www.societe-informatique-de-france.fr/wp-content/uploads/2019/04/1024-numero-13_Article9.pdf)


Contacts
--------

Responsable de la formation : Benoit Papegay, secondé par Philippe Marquet \
2e étage de l'extension du bâtiment M3 \
[📧 diu-eil@univ-lille.fr](mailto:diu-eil@univ-lille.fr)

Secrétariat pédagogique : Jessica Barret et Pierre Rigolot \
rez-de-chaussée du bâtiment M3




